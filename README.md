# README #

Studio TEM's Starter theme for Wordpress custom development

### Install ###

* pull repository to your local enviroment
* change remote origin to project repository
* create database
* cp .env-sample to .env
* set up your database connections
* set WP_HOME and WP_SITEURL to your localhost URL
* continue with classic Wordpress install
* custom_theme has some default theme settings our company is expecting, and some prepared file structure, feel free to extend it as project needs

### .env and wp-config.php ###

* generate new Salts for config, otherwise wp-config.php should not be changed and commited
* all enviroment specific value and access credentials should be set in .env file
* use constant ENVIROMENT_IS_PRODUCTION for conditions allowing production only functions, like running GTM script only on production enviroment
* it also automatically turn on/off Search engine visibility, so it doesnt need to be changed in database on migrations
* IS_LOCALHOST enables features important for development

### Additional info ###

* .gitignore should be set up properly for Wordpress needs, but feel free to edit it if needed
* remember that, because of automatic deployments there can not be any dynamic changes on server
* no updates can be done server, git push from server is not available because web runs on 2-3 lxc containers simultaniously
* be sure to check files in custom_theme/inc/ folder, edit them as your project needs

### For any questions ###

* [Contact email](mailto:mihalciny@studiotem.com)
