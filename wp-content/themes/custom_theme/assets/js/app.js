 
(function($){

  // Init Skrollr
  //var s = skrollr.init();
 
  // Refresh Skrollr after resizing our sections
 // s.refresh(jQuery('home-about-parallax')); 
 var scroll = jQuery(window).scrollTop();     
     if ( scroll >= 150 ) {
        $("body").addClass("scrolled");
    } else {
        $("body").removeClass("scrolled");
    } 

$(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();

    if ( scroll >= 150 ) {
        $("body").addClass("scrolled");
        $("body").addClass("scrolled");
        jQuery("#wpfront-scroll-top-container").fadeIn(500);        
    } else {
        $("body").removeClass("scrolled");
        $("body").removeClass("scrolled");
        jQuery("#wpfront-scroll-top-container").fadeOut(500);         
    } 
});

  // Run the function in case of window resize
 if ( document.documentElement.clientWidth > '667' ) {
    jQuery('section.products .content-wrapper').matchHeight({
     byRow: true,
     //target: '.item',
    });     
 }     
   

  $(document).ready(function(){

    /** 
     * This part does the "fixed navigation after scroll" functionality
     * We use the jQuery function scroll() to recalculate our variables as the 
     * page is scrolled/
     */     
    $(window).scroll(function(){
        var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
        if( jQuery('#nav-anchor').length ) {

            var div_top = jQuery('#nav-anchor').offset().top;
            if (window_top > div_top) {
                $('.menu-wrapper').addClass('stick');
            } else {
                $('.menu-wrapper').removeClass('stick');
            }
        
        }
    });
    
  jQuery('.main-body a[href^="#"]').each(function(){
      if( jQuery(this).attr('href').length > 2 ) {
        jQuery(this).addClass('vertical');
      }
  });
 
     

  $('#main-navigation a[href^="#"]').click(function(e) {
    // Prevent the jump and the #hash from appearing on the address bar
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);       
      //var distance_top_without_header = $target.offset().top - 88;
      
        if( jQuery('body').hasClass('home') ) {
        var distance_top_without_header = $target.offset().top - $('#header').outerHeight(true);
	       $('html, body').stop().animate({
	           'scrollTop': distance_top_without_header
	       }, 600, 'swing' );
        } else {
            
            //go to homepage
            var base_url = window.location.origin;
            console.log(base_url);
             console.log(target);
            window.location.href = base_url +"/" + target;
            
        }
 
  });    
  
  jQuery('.footer-widgets-col a[href*="#"]').click(function(e) {
    // Prevent the jump and the #hash from appearing on the address bar
	    e.preventDefault();

        jQuery(this).addClass('aa');
	    var target = this.hash;
	    var $target = $(target);       
      //var distance_top_without_header = $target.offset().top - 88;
      
        if( jQuery('body').hasClass('home') ) {
        var distance_top_without_header = $target.offset().top - $('#header').outerHeight(true);
	       $('html, body').stop().animate({
	           'scrollTop': distance_top_without_header
	       }, 600, 'swing' );
        } else {
            
            //go to homepage
            var base_url = window.location.origin;
            console.log(base_url);
             console.log(target);
            window.location.href = base_url +"/" + target;
            
        }
 
  });  
 
	$(document).on('click tap','a.vertical, li.vertical > a, li.mega-vertical > a', function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);
      //var distance_top_without_header = $target.offset().top - $('#main-header').outerHeight(true);
      var distance_top_without_header = $target.offset().top - 88;

	    $('html, body').stop().animate({
	        'scrollTop': distance_top_without_header
	    }, 600, 'swing' );
      
      
	});


  
  }); //document ready

 $(document).on('click tap','#ham', function (e) {
    // everything looks good!    
    e.preventDefault();
    var mobile_menu = jQuery('#mobile-menu');
    
    if( jQuery(this).hasClass('open') ) {
      jQuery(this).removeClass('open');
      jQuery('body').removeClass('nav-is-toggled');     
      jQuery(mobile_menu).fadeOut(400); 
    } else {
      jQuery(this).addClass('open');
      jQuery('body').addClass('nav-is-toggled');  
      jQuery(mobile_menu).fadeIn(400);      
    }
    return false; 
 });    
 
 $(document).on('click tap','.gallery-images .each-image a', function (e) {
    // everything looks good!    
    e.preventDefault();
    var new_src = jQuery(this).attr('data-url');
    var old_src = jQuery('.intro-image-wrapper img').attr('src');
    if( new_src != old_src ) {
 
            jQuery('.intro-image-wrapper .img-wrapper img').attr('src', new_src).attr('srcset', '').attr('sizes', '').attr('src', new_src);
    
    
    }
    return false; 
 });       
  

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // OWL Carousel
 
 function mobile_testimonials_carousel() {
            var checkWidth = window.innerWidth;
            var testimonials_carousel = jQuery("#testimonials-carousel");
            if (checkWidth >= 800) {
                //testimonials_carousel.data('owlCarousel').destroy();
                testimonials_carousel.removeClass('owl-carousel');
            } else if (checkWidth < 800) {
                testimonials_carousel.addClass('owl-carousel');
      var testimonials_carousel = $('#testimonials-carousel');
      testimonials_carousel.owlCarousel({
    autoHeight:false,
    center: false,
    items:3,
    loop:true,
    margin:0,
    dots:true,
    autoplayHoverPause: true,
    autoplaySpeed: 1200,
    autoplayTimeout: 4500, 
    navSpeed: 500,  
    smartSpeed:500, 
    slideSpeed: 500,
    paginationSpeed: 500,
    responsive:{    
        1024:{
            items:3,
            center: false,            
        }, 
        768:{
            items:2,
            center: false,            
        },           
        0:{
            items:1,
            center: false,
        }        
    },
      });
            }
        }

        jQuery(document).ready(mobile_testimonials_carousel);
        jQuery(window).resize(mobile_testimonials_carousel); 
 
 


      var products_carousel = $('#product-carousel');
      products_carousel.owlCarousel({
    autoHeight:false,
    center: false,
    items:2,
    loop:true,
    margin:0,
    dots:false,
    autoplay: false,
    autoplayHoverPause: true,
    autoplaySpeed: 1200,
    autoplayTimeout: 4500, 
    navSpeed: 500,  
    smartSpeed:500, 
    slideSpeed: 500,
    paginationSpeed: 500,
    responsive:{    
        1024:{
            items:2,
            center: false,            
        }, 
        667:{
            items:2,
            center: false,            
        },           
        0:{
            items:1,
            center: false,
        }        
    },
      });
        $('.products-carousel-custom-nav a.next-link').click(function(e) {
            e.preventDefault();
            products_carousel.trigger('next.owl.carousel');
        });
    // Go to the previous item
        $('.products-carousel-custom-nav a.prev-link').click(function(e) {
            e.preventDefault();
            products_carousel.trigger('prev.owl.carousel');
        });     
   
  
})(jQuery);