<?php
/**
 * Produkty template
 *
 * @package WordPress
 * @subpackage custom_theme
 * @author Studio TEM
 */

get_header();

?>

    <section class="product-intro">
       <div class="container">
         <div class="row">
            <div class="section-i">
                <div class="flex-columns col2">
                    <div class="flex-column text">
                     <div class="content-wrapper">
                      <h1><?php the_title(); ?></h1>
                      <div class="description"><p class=""><?php the_field('short_description'); ?></div>
                      <div class="price"><?php the_field('price'); ?> &euro;</div>  
                      <div class="content"><?php the_content(); ?></div> 
                      <div class="buy-link"><a href="<?php the_field('buy_link'); ?>" target="_blank"><?php _e('Buy on Amazon','custom_theme') ?></a></div>                                                               
                     </div>
                    </div>
                    <div class="flex-column imagery">                    
                      <?php if( "" != get_the_post_thumbnail() ) { ?>  
                        <div class="intro-image-wrapper">                            
                            <div class="img-wrapper"><?php the_post_thumbnail('full'); ?></div>                                                             
                            <div class="gallery-images">
                              <div class="inner-content">
                                <div class="each-image"><a href="#" data-url="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>" rel="nofollow"><?php the_post_thumbnail('thumbnail'); ?></a></div>        
                      <?php
                        if( have_rows('gallery') ):
                            while( have_rows('gallery') ) : the_row();
                                $each_image = get_sub_field('gallery_image');
                                //var_dump($each_image);
                      ?>
                        
                            <div class="each-image"><a href="#" data-url="<?php echo $each_image['url']; ?>" rel="nofollow"><img src="<?php echo $each_image['sizes']['thumbnail']; ?>"><span class="preload" style="display:none !important"><img src="<?php echo $each_image['url']; ?>"></span></a></div>
                      
                      <?php
                            endwhile;
                        endif;
                      ?>        
                              </div>                        
                            </div>
                        </div> 
                      <?php } ?>                          
                    </div>
                </div>               
            </div>   
         </div> 
       </div>            
        
    
    </section>

 
	<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
 
				//the_content();
                

// Check value exists.
if( have_rows('flexible_content') ):

    // Loop through rows.
    while ( have_rows('flexible_content') ) : the_row();
 
            get_template_part( 'templates/partials/flexible', get_row_layout() );


    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; 

                
			}
		}
	?>
 

<?php

get_footer();
