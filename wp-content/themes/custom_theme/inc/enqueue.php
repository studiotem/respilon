<?php

/************************************************************************************/
/* Enqueue scripts and styles. */
/************************************************************************************/

/* Frontend */
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
function custom_scripts() {

	// disables some new guttenberg features and default wp jquery
	wp_enqueue_style( 'jquery' );
	//wp_dequeue_script( 'jquery-blockui' );
	wp_deregister_script( 'wp-embed' );
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wc-block-vendors-style' );
	wp_dequeue_style( 'wc-block-style' );

	$dir = get_template_directory_uri();
	if ( IS_LOCALHOST ) {
		$version = time();
	} else {
		$the_theme = wp_get_theme();
		$version = $the_theme->get( 'Version' );
	}

	// styles
	wp_enqueue_style( 'main_styles', $dir . '/assets/css/main.css', array(), $version );    
	wp_enqueue_style( 'custom_styles', $dir . '/assets/css/app.css', array(), $version );
	wp_enqueue_style( 'theme_styles', $dir . '/style.css', array(), $version );

	// scripts
    //wp_enqueue_script( 'jquery' );
	//wp_enqueue_script( 'main_scripts', $dir . '/assets/js/main.js', array(), $version, true );   
    
	// Owl Carousel Full style+script
	wp_enqueue_style( 'jquery_owl_style', $dir . '/assets/css/owl.carousel.min.css', array(), $version );
	wp_enqueue_script( 'jquery_owl_slider', $dir . '/assets/js/owl.carousel.min.js', array(), $version, true );   
     
	wp_enqueue_script( 'localScroll', $dir . '/assets/js/jquery.localScroll.min.js', array(), $version, true );  
	wp_enqueue_script( 'matchHeight', $dir . '/assets/js/jquery.matchHeight.js', array(), $version, true );     
	wp_enqueue_script( 'scrollTo', $dir . '/assets/js/jquery.scrollTo.min.js', array(), $version, true );     
	wp_enqueue_script( 'custom_scripts', $dir . '/assets/js/app.js', array(), $version, true );

	// cookies library
	wp_enqueue_script( 'cookie_script', $dir . '/libraries/cookieconsent.min.js', array(), $version, true );
	wp_enqueue_script( 'cookie_init', $dir . '/libraries/cookieconsent.init.js', array( 'cookie_script' ), $version, true );
	wp_enqueue_style( 'cookie_style', $dir . '/libraries/cookieconsent.min.css', array(), $version );

	// localized js objects
	if ( function_exists( 'get_field' ) ) {
		wp_localize_script( 'cookie_init', 'cookies',
			array(
				'disclaimer'	=> get_field( 'disclaimer', 'options' ),
				'accept_label'	=> get_field( 'accept_label', 'options' ),
				'more_label'	=> get_field( 'more_label', 'options' ),
				'more_url'		=> get_field( 'more_url', 'options' )
			)
		);
	}

}


/* Backend */
add_action( 'admin_enqueue_scripts', 'admin_scripts' );
function admin_scripts() {

	$dir = get_template_directory_uri();
	if ( IS_LOCALHOST ) {
		$version = time();
	} else {
		$the_theme = wp_get_theme();
		$version = $the_theme->get( 'Version' );
	}

	// simpler wp admin
	wp_enqueue_style( 'style_admin', $dir . '/style_admin.css', array(), $version );

}


/* Languages */
function child_theme_text_domain() {
	load_child_theme_textdomain( 'custom_theme', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'child_theme_text_domain' );
