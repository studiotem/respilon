<?php

/************************************************************************************/
/* Google Analytics */
/************************************************************************************/
add_action( 'wp_head', 'analytics' );
function analytics() {
	
    if ( ! ENVIROMENT_IS_PRODUCTION ) {
		return;
	}

    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
    $parse = parse_url($actual_link);
    $website_url = $parse['host'];

    if( strpos($website_url, '.com') !== false ) {
        //COM
    ?>
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-49QJPMBCK5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-49QJPMBCK5');
</script> 
    
    <?php
    } else if ( strpos($website_url, '.de') !== false ) {
        //DE
    ?>
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-CVCZF49L78"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-CVCZF49L78');
</script> 
    
    <?php
    } else if ( strpos($website_url, '.at') !== false ) {
      //AT
    ?>
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-MVQ7GCGDN5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-MVQ7GCGDN5');
</script>
    
    <?php
    } 
    
        
    
    
    
}
