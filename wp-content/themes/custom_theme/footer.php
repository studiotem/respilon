<?php
/**
 * Footer
 *
 * @package WordPress
 * @subpackage custom_theme
 * @author Studio TEM
 */
?>

        <?php 
              $args = array();
              get_template_part( 'templates/partials/footer', 'default', $args );
        ?>    


    <?php wp_footer() ?>
    
    <style>
        html { margin-top:0 !important; }
 
    </style>
</body>
</html>
