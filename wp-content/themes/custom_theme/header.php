<?php
/**
 * Header
 *
 * @package WordPress
 * @subpackage custom_theme
 * @author Studio TEM
 */
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ) ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="author" content="Studio TEM" />

	<?php wp_enqueue_script("jquery"); ?>
    <?php wp_head() ?>

</head>
<body <?php body_class() ?>>
	<?php do_action( 'wp_body_open' ); ?>
    
    
    
        <?php 
              $args = array();
              get_template_part( 'templates/partials/header', 'default', $args );
        ?>    
