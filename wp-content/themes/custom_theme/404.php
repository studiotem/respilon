<?php
/**
 *
 * Default page template 
 *
 * 
 */


get_header();

    $image = get_field('404_image','option');
    $image_url = $image['url'];
    $id =  get_sub_field('id');  

?>

<div class="default-page-body page-404">
  <div class="">  
    <div class="row">
      <div class="inner-content">
        <div class="title-wrapper">
            <div class="title-404">404</div>
            <div class="image"><img src="<?php echo $image_url; ?>"></div>
        </div>
        <div class="products-link"><a href="<?php echo site_url(); ?>/<?php echo get_field('404_products_link', 'option'); ?>"><?php _e('mehr zum Produkt','custom_theme'); ?></a></div>        
        <div class="decor"></div>
      </div>        
    </div>
  </div>
</div>  
    

<?php

get_footer();
