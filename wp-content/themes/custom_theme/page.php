<?php
/**
 *
 * Default page template 
 *
 * 
 */


get_header();

?>

<div class="default-page-body">
  <div class="container">  
    <div class="row">
        <?php the_content(); ?>
    </div>
  </div>
</div>  
    

<?php

get_footer();
