<?php 

    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
    $id =  get_sub_field('id');     


?>

    <section class="filter <?php echo $background; ?>" id="<?php echo $id; ?>">
       <div class="container">
         <div class="row">
            <div class="section-i">
                <div class="flex-columns col2">
                    <div class="flex-column text">
                      <div class="content-wrapper">
                        <?php echo wpautop( get_sub_field('text') ); ?>
                      </div>
                    </div>
                    <div class="flex-column image">
                      <?php if( $image_url ) { ?>  
                        <div class="intro-image-wrapper">                            
                            <div class="img-wrapper"><img src="<?php echo $image_url; ?>" alt="<?php echo $image['alt']; ?>"></div>                                                             
                            <div class="hero--image--hover d-none d-lg-block">
                      <?php
                        if( have_rows('info_points') ):
                            while( have_rows('info_points') ) : the_row();
                      ?>
                        
                        <div class="hover" style="left:<?php echo get_sub_field('position_left'); ?>; top:<?php echo get_sub_field('position_top'); ?>;"><div class="pulse"></div><div class="inner"></div><div class="tolltip"><?php echo get_sub_field('description'); ?></div></div>
                      
                      <?php
                            endwhile;
                        endif;
                      ?>                                
                            </div>
                        </div> 
                      <?php } ?>                          
                    </div>
                </div>               
            </div>   
         </div> 
       </div>
    </section>
