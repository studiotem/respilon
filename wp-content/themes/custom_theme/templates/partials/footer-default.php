<?php 

    // Default Footer
    $logo = get_field('footer_logo', 'option');
    $logo_image = $logo['url'];

?>

</div><!-- main-body -->

<footer class="footer ">
    <div class="footer--info">
        <div class="container">
            <div class="row">
                <div class="logo col-12"><a href="<?php echo site_url(); ?>"><img src="<?php echo $logo_image; ?>" class="tall" alt="Respilon"></a></div>
            </div>
        </div>
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="footer-widgets-col footer-col-1"> 
                        <?php 
                            if( get_field('widget_1_title', 'option') ) : echo "<h6>". get_field('widget_1_title', 'option'). "</h6>"; endif;
                        ?>
                        <div class='columns'>
                       <?php 
                            if( get_field('widget_1_column_1', 'option') ) :
                        ?>
                            <div class="single-col-1"><?php echo wpautop( get_field('widget_1_column_1', 'option') ); ?></div>
                        <?php    
                            endif;
                        ?>   
                       <?php 
                            if( get_field('widget_1_column_2', 'option') ) :
                        ?>
                            <div class="single-col-2"><?php echo wpautop( get_field('widget_1_column_2', 'option') ); ?></div>
                        <?php    
                            endif;
                        ?>  
                        </div>                                            
                    </div>  
                    <div class="footer-widgets-col footer-col-2"> 
                        <?php 
                            if( get_field('widget_2_title', 'option') ) : echo "<h6>". get_field('widget_2_title', 'option'). "</h6>"; endif;
                        ?>
                        <div class='columns'>
                       <?php 
                            if( get_field('widget_2_column_1', 'option') ) :
                        ?>
                            <div class="single-col-1"><?php echo wpautop( get_field('widget_2_column_1', 'option') ); ?></div>
                        <?php    
                            endif;
                        ?>   
                       <?php 
                            if( get_field('widget_2_column_2', 'option') ) :
                        ?>
                            <div class="single-col-2"><?php echo wpautop( get_field('widget_2_column_2', 'option') ); ?></div>
                        <?php    
                            endif;
                        ?>      
                        </div>                                        
                    </div>  
                    <div class="footer-widgets-col footer-col-3"> 
                       <?php
                            if( get_field('widget_3_title', 'option') ) : echo "<h6>". get_field('widget_3_title', 'option'). "</h6>"; endif;
                       ?>
                       <?php if( get_field('facebook_link', 'option') ) : ?><div class="social-link facebook-link"><a href="<?php echo get_field('facebook_link', 'option'); ?>"><?php echo get_field('facebook_name', 'option'); ?></a></div><?php endif; ?>   
                       <?php if( get_field('instagram_link', 'option') ) : ?><div class="social-link instagram-link"><a href="<?php echo get_field('instagram_link', 'option'); ?>"><?php echo get_field('instagram_name', 'option'); ?></a></div><?php endif; ?>
                    </div>                                                         
                </div>
            </div>    
        </div>  
        
        <div class="copyright-row">
            <div class="container">
                <div class="row">
                    <div class="copyright-left"><div class="cop"><?php echo get_field('copyright_left', 'option'); ?></div></div> 
                    <div class="copyright-right"><div class="vop"><?php echo get_field('copyright_right', 'option'); ?></div></div> 
                </div>
            </div>    
        </div>
          
    </div> 
          
</footer>