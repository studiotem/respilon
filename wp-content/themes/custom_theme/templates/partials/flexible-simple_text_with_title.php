
    <section class="simple-text" id="<?php echo get_sub_field('id'); ?>" style=" ">
       <div class="container">
         <div class="row">
            <div class="inner-content">
                <h2 class="section-title"><?php echo get_sub_field('title'); ?></h2>
                <?php echo wpautop( get_sub_field('text') ); ?> 
 
            </div>   
         </div> 
       </div>
    </section>
