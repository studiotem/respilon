<?php 
    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
?>

    
    <section class="graph" id="<?php echo get_sub_field('id'); ?>" style="background-color: #F8F8F8;">
       <div class="section-w">
         <div class="row">
            <div class="section-i">
                <h2 class="section-title"><?php echo get_sub_field('title'); ?></h2>
                <?php echo wpautop( get_sub_field('text') ); ?>
                <div class="particles-graph">
                  <div class="particles-graph-i">
                    <img src="<?php echo $image_url; ?>" alt="Graph image">
                  </div>
                </div>                
            </div>   
         </div> 
       </div>
    </section>       