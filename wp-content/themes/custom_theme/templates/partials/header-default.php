        <?php 
            
    $logo = get_field('logo', 'option');
    $logo_image = $logo['url'];
        
        ?>
        <header id="header" class="sticky" data-scroll-header style="background-color: rgba(255, 255, 255, 0);">      
                 
          <div class="menu-wrapper menu-<?php echo $menu_color_type; ?> container">          
              <div class="row"> 
                <div class="menu-inner">

                  <div class="header-logo">
                   <a href="<?php echo site_url(); ?>"><img src="<?php echo $logo_image; ?>" class="tall"></a>
                  </div>                   
                
                  <div class="menu-holder header-menu">
                    <div class="" id="navbarResponsive">
                     <nav>
                   <?php
                    $main_nav_args = array(
	                   'theme_location'  => 'main_navigation',
	                   'menu'            => 'main_navigation',
	                   'container'       => '',
	                   'menu_class'      => 'menu',
	                   'menu_id'         => 'main-navigation',
	                   'echo'            => true,
                       'walker'            => new Respilon_Custom_Nav_Walker(),// custom walker class.                       
	                   'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul><div class="cl-fl"></div>'
                    );
                    wp_nav_menu( $main_nav_args );
                   ?>
                      </nav>
                    </div> 
                  </div>                
                                  
                  <div class="mobile-menu ml-auto">
                    <div class="nav-top" id="ham"><span></span> <span></span></div>
                    <nav class="nav-drill">
                   <?php
                    $main_nav_args = array(
	                   'theme_location'  => 'main_navigation',
	                   'menu'            => 'main_navigation',
	                   'container'       => '',
	                   'menu_class'      => 'menu',
	                   'menu_id'         => 'mobile-navigation',
	                   'echo'            => true,
                       'walker'            => new Respilon_Custom_Nav_Walker(),// custom walker class.                       
	                   'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul><div class="cl-fl"></div>'
                    );
                    wp_nav_menu( $main_nav_args );
                   ?>                    
                    </nav>
                  </div>
                                                                      
                 
                                              
 
                </div>  
 
                               
               </div>                          
            <div class="cl-fl"></div>
 
            
          </div>

          
        </header>
        
        <div class="main-body">