<?php 
        $title = get_sub_field('title') ? get_sub_field('title') : __('Contact Us','custom_theme') ;
        $description = get_sub_field('description') ? get_sub_field('description') : __('Do you have any questions or would you like to tell us something? Please use our contact form.','custom_theme') ;

?>    
    
    <section class="contact" id="<?php echo get_sub_field('id'); ?>" style=" ">
       <div class="section-w">
         <div class="row">
            <div class="contact-form">
                <h2 class="section-title"><?php echo $title; ?></h2>
                <?php echo wpautop( $description ); ?>
                
                <?php 
                    $contact_form = get_sub_field('contact_form');
                    $contact_form_id = $contact_form->ID;
                    echo do_shortcode('[contact-form-7 id="'.$contact_form_id.'"]');    
                    
                
                ?>
                
            </div>   
         </div> 
       </div>
    </section>  
