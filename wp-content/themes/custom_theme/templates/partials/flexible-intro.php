<?php 
    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
?>

    <section class="intro" id="<?php echo get_sub_field('id'); ?>">
       <div class="section-w">
        <div class="container">
         <div class="row">
            <div class="section-i">
                <div class="flex-columns col2">
                    <div class="flex-column">
                      <div class="content-wrapper">
                        <h1><?php echo get_sub_field('title'); ?></h1>  
                        <p><?php echo get_sub_field('description'); ?></p>  
                        <p><a href="<?php echo get_sub_field('product_link'); ?>" class="vertical button btn-def"><?php _e('mehr zum Produkt','custom_theme'); ?></a></p>
                        <div class="arrow-down"><a href="<?php echo get_sub_field('next_section_link'); ?>" class="vertical"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Arrow-1.png" alt="arrow down image"></a></div>
                      </div>
                    </div>
                    <div class="flex-column last">
                        <div class="intro-image-wrapper">
                            <div class="img-wrapper"><img src="<?php echo $image_url; ?>" alt="<?php echo $image['description']; ?>"></div> 
                            <div class="rounded-background type-1"></div>    
                        </div>                           
                    </div>
                </div>               
            </div>   
         </div> 
        </div>
       </div>
    </section>   
      
       