<?php 
    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
?>

    <section class="testimonials" id="<?php echo get_sub_field('id'); ?>" >
       <div class="section-w">
         <div class="row">
            <div class="section-i">
                <h2 class="section-title"><?php echo get_sub_field('title'); ?></h2>

                      <?php
                        if( have_rows('testimonials') ):
                      ?>
                  <div id="testimonials-carousel" class="owl-carousel owl-theme">                        
                      <?php
                            while( have_rows('testimonials') ) : the_row();
                                $each_image = get_sub_field('person_image');
                                //var_dump($each_image);
                      ?>
                        
                    <div class="item">
                        <div class="testimonial-content">   
                         <div class="quote-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/quote-icon.png" alt="quote icon"></div>                                         
                         <div class="text"><?php echo get_sub_field('text'); ?></div>
                         <div class="testimonial-image"><img src="<?php echo $each_image['url']; ?>" alt="<?php echo get_sub_field('author'); ?> portrait"></div>
                         <div class="name"><?php echo get_sub_field('author'); ?></div>    
                        </div>
                    </div>
                      
                      <?php
                            endwhile;
                      ?>
                  </div>    
                      <?php
                        endif;
                      ?>                        
 
                                                                                  
 
            </div>   
         </div> 
       </div>
    </section>    