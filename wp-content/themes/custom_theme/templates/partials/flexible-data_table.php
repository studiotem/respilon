
    <section class="data-table" id="" style=" ">
       <div class="container">
         <div class="row">
            <h2><?php echo get_sub_field('title'); ?></h2>
            <div class="data-table-inner">                
                <?php echo do_shortcode( get_sub_field('table_shortcode') ); ?> 
            </div>   
         </div> 
       </div>
    </section>
