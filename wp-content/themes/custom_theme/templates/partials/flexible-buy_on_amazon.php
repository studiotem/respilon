<?php 
        $title = get_sub_field('title') ? get_sub_field('title') : __('Buy on Amazon','custom_theme') ;
        $link = get_sub_field('link') ? get_sub_field('link') : '#';

?>    
    
    <section class="buy-cta" id="" style=" ">
       <div class="container">
         <div class="row">
                <h2 class="section-title"><?php echo $title; ?></h2>
                <div class="button-holder"><a href="<?php echo $link; ?>" target="_blank"><div><span></span><?php _e('Buy on Amazon','custom_theme'); ?></div></a></div>
         </div> 
       </div>
    </section>  
