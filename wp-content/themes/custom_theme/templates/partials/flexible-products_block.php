<?php 

    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
    $id =  get_sub_field('id');     


?>

    <section class="products" id="Produkte">
       <div class="container">
         <div class="row">
            <div class="section-i">
                <div class="section-title"><h1><?php echo get_sub_field('title'); ?></h1></div>                         
                
                <div class="flex-columns col2">
                <?php 
                  $all_products = get_posts( array(
                    'posts_per_page' => -1,
                    'orderby'        => 'menu_order',
                    'post_type'      => 'produkty',
                    'post_status'    => 'publish',
                    'suppress_filters' => false,                    
                 ) );
                 if ( $all_products ) {
                ?> 
                <?php
                    foreach ( $all_products as $post ) {
                        setup_postdata( $post );
                ?>
                    <div class="flex-column">
                     <div class="main-area">                    
                      <div class="image-wrapper">
                        <div class="product-image"><?php the_post_thumbnail(); ?></div>    
                      </div>                    
                      <div class="content-wrapper">  
                        <div class="cw-flex">
                            <div class="cw-info">
                                <div class="price"><?php the_field('price'); ?> &euro;</div>
                                <div class="title"><h3><?php the_title(); ?></h3></div>
                                <div class="desc"><?php wpautop(the_field('short_description')); ?></div>
                            </div>
                            <div class="cw-cta">
                                <div class="detail"><a href="<?php echo get_permalink(); ?>"><?php _e('Product Detail','custom_theme') ?></a></div>
                                <div class="buy-link"><a href="<?php the_field('buy_link'); ?>" target="_blank"><?php _e('Buy on Amazon','custom_theme') ?></a></div>
                            </div>    
                        </div>   
                      </div>
                     </div>
                    </div>                
                <?php       
                    }
                ?>
                <?php
                    wp_reset_postdata();
                }
                ?>
                </div>                   
                
                <div class="products-carousel-wrapper">
                <?php 
                  $all_products = get_posts( array(
                    'posts_per_page' => -1,
                    'orderby'        => 'menu_order',
                    'post_type'      => 'produkty',
                    'post_status'    => 'publish',
                    'suppress_filters' => false,
                 ) );
                 if ( $all_products ) {
                ?>
                    
                    <div class="products-carousel-custom-nav">
                        <a href="#" rel="nofollow" class="prev-link"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Arrow-2.png"></a>
                        <a href="#" rel="nofollow" class="next-link"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Arrow-2-right.png"></a>
                    </div>
                    
                    <div id="product-carousel" class="owl-carousel owl-theme"> 
                <?php
                    foreach ( $all_products as $post ) {
                        setup_postdata( $post );
                ?>
                    <div class="item">
                     <div class="main-area">                    
                      <div class="image-wrapper">
                        <div class="product-image"><?php the_post_thumbnail(); ?></div>    
                      </div>                    
                      <div class="content-wrapper">  
                        <div class="cw-flex">
                            <div class="cw-info">
                                <div class="price"><?php the_field('price'); ?> &euro;</div>
                                <div class="title"><h3><?php the_title(); ?></h3></div>
                                <div class="desc"><?php wpautop(the_field('short_description')); ?></div>
                            </div>
                            <div class="cw-cta">
                                <div class="detail"><a href="<?php echo get_permalink(); ?>"><?php _e('Product Detail','custom_theme') ?></a></div>
                                <div class="buy-link"><a href="<?php the_field('buy_link'); ?>" target="_blank"><?php _e('Buy on Amazon','custom_theme') ?></a></div>
                            </div>    
                        </div>   
                      </div>
                     </div>
                    </div>                
                <?php       
                    }
                ?>
                    </div>
                <?php
                    wp_reset_postdata();
                }
                ?>
                </div>     
            </div>   
         </div> 
       </div>
    </section>