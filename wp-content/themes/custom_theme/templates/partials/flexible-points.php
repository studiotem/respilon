<?php 
    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
?>

<section class="points" id="Wohltatigkeit">
       <div class="section-w">
        <div class="container">
         <div class="row">
            <div class="section-i">
                <div class="section-title"><h1><?php echo get_sub_field('title'); ?></h1></div>
                <div class="flex-columns col4">
                      <?php
                        if( have_rows('points') ):
                            while( have_rows('points') ) : the_row();
                                $each_image = get_sub_field('icon');
                                //var_dump($each_image);
                      ?>
                        
                    <div class="flex-column">
                      <div class="content-wrapper">
                        <div class="icon"><img src="<?php echo $each_image['url']; ?>" alt=" image"></div>  
                        <p><strong><?php echo get_sub_field('text'); ?></strong></p>  
                      </div>
                    </div>                    
                      
                      <?php
                            endwhile;
                        endif;
                      ?>                                            
                </div>                
            </div>   
         </div>
        </div> 
       </div>
    </section>
      
       