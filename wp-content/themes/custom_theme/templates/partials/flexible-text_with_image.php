<?php 
    $background = 'bg-'.get_sub_field('background'); 
    $image_position = get_sub_field('image_position');
    $image_position = ( $image_position ) ? strtolower( $image_position ) : 'left' ;
    $image = get_sub_field('image');
    $image_url = $image['url'];
?>

    <section class="text-with-image <?php echo $background; ?> image-<?php echo $image_position; ?>" id="" style=" ">
       <div class="container">
         <div class="row">
            <div class="flex-columns">
                <div class="flex-column text"> 

                    <div class="content-inner">
                        <h2 class="section-title"><?php echo get_sub_field('title'); ?></h2>
                        <div class="text"><?php echo wpautop( get_sub_field('text') ); ?></div> 
 
                    </div>                
                
                </div>  
                <div class="flex-column image"> 
                    <?php if( $image_url ) : ?> <img src="<?php echo $image_url; ?>" alt="<?php echo $image['alt']; ?>"> <?php endif; ?>
                </div>                 
            </div>
               
         </div> 
       </div>
    </section>
