<?php
/**
 * custom_theme functions include
 *
 * @package WordPress
 * @subpackage custom_theme
 * @author Studio TEM
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$includes = array(
	'/setup.php',
	'/enqueue.php',
	'/acf_theme_options.php',
	'/extras.php',
	'/tracking_scripts.php',
	'/menu.php',
	'/image_sizes.php',
);

foreach ( $includes as $file ) {
	require_once get_stylesheet_directory() . '/inc' . $file;
}

register_nav_menus( array(
	'main_navigation' => 'Main Navigation',              
	'mobile_navigation' => 'Mobile Navigation',  
	'footer_navigation' => 'Footer Navigation',     
) );

function sidebars_init() {

    register_sidebar(array(
		'name' => __( 'Blog Sidebar' ),
		'id' => 'blog_sidebar',
		'description' => __( 'Widgets in the Sidebar' ),
		'before_widget' => '<aside class="widget">',
		'after_widget' => '</aside>',
		'before_title' => '<h6 class="sidebar-widget-title">',
		'after_title' => '</h6>',
	   ));
 
    register_sidebar(array(
		'name' => __( 'Footer Top Row' ),
		'id' => 'footer_row_top',
		'description' => __( 'Widgets in the Footer Top Row' ),
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div><div class="cl-fl"></div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	   ));  
    register_sidebar(array(
		'name' => __( 'Footer Column 1' ),
		'id' => 'footer_column_1',
		'description' => __( 'Widgets in the Footer' ),
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div><div class="cl-fl"></div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	   ));   
    register_sidebar(array(
		'name' => __( 'Footer Column 2' ),
		'id' => 'footer_column_2',
		'description' => __( 'Widgets in the Footer' ),
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div><div class="cl-fl"></div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	   ));   
    register_sidebar(array(
		'name' => __( 'Footer Column 3' ),
		'id' => 'footer_column_3',
		'description' => __( 'Widgets in the Footer' ),
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div><div class="cl-fl"></div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	   ));   
    register_sidebar(array(
		'name' => __( 'Footer Copyright Notice' ),
		'id' => 'footer_copyright',
		'description' => __( '' ),
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	   ));  
    register_sidebar(array(
		'name' => __( 'Footer Bottom Menu' ),
		'id' => 'footer_bottom_menu',
		'description' => __( '' ),
		'before_widget' => '<div class="footer-privacy-widget">',
		'after_widget' => '</div><div class="cl-fl"></div>',
		'before_title' => '<div>',
		'after_title' => '</div>',
	   ));                                               	
                                     
}
add_action( 'widgets_init', 'sidebars_init' );

if (!class_exists('Respilon_Custom_Nav_Walker')) {
class Respilon_Custom_Nav_Walker extends Walker_Nav_Menu { 

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) { 
        if (!$element)
            return;
        $id_field = $this->db_fields['id'];

        if (is_array($args[0]))
            $args[0]['has_children'] = !empty($children_elements[$element->$id_field]);
        else if (is_object($args[0]))
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        $cb_args = array_merge(array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'start_el'), $cb_args);

        $id = $element->$id_field;
        if (($max_depth == 0 || $max_depth > $depth + 1) && isset($children_elements[$id])) {

            foreach ($children_elements[$id] as $child) {

                if (!isset($newlevel)) {
                    $newlevel = true;
                    $cb_args = array_merge(array(&$output, $depth), $args);
                    call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
                }
                $this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
            }
            unset($children_elements[$id]);
        }

        if (isset($newlevel) && $newlevel) {
            $cb_args = array_merge(array(&$output, $depth), $args);
            call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
        }

        $cb_args = array_merge(array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'end_el'), $cb_args);
    }

    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) { 
        if ((is_object($item) && $item->title == null) || (!is_object($item))) {
            return ;
        }

        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $li_attributes = '';
        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array) $item->classes;
        if (is_object($args) && $args->has_children) {
            //$classes[] = 'dropdown';
            $li_attributes .= ' data-dropdown="dropdown"';
        }
        $classes[] = 'menu-item-' . $item->ID;
        $classes[] = ($item->current) ? 'active' : '';
        $classes[] = 'nav-item';

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        
        $class_names = ' class="' . esc_attr($class_names) . '"';
                

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        $attributes .= (is_object($args) && $args->has_children) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

        $item_output = (is_object($args)) ? $args->before : '';
        $item_output .= '<a' . $attributes . ' class="nav-link nav-scroll">';
        $item_output .= (is_object($args) ? $args->link_before : '') . apply_filters('the_title', $item->title, $item->ID) . (is_object($args) ? $args->link_after : '');
        $item_output .= (is_object($args) && $args->has_children) ? ' <span class="caret"></span> ' : '';
        $item_output .= '</a>';
        $item_output .= (is_object($args) ? $args->after : '');

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }


    public function start_lvl(&$output, $depth = 0, $args = array()) { 
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
    }


	}
}


function get_flexible_content() {
    
// Check value exists.
if( have_rows('flexible_content') ):

    // Loop through rows.
    while ( have_rows('flexible_content') ) : the_row();
 
            get_template_part( 'templates/partials/flexible', get_row_layout() );


    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;      

}
